using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class interact : MonoBehaviour
{
    public Text interactText;
    public action actionToDo; //base class
    void Start()
    {
        interactText.gameObject.SetActive(false);
    }
    // Start is called before the first frame update
    void OnTriggerEnter()
    {
        interactText.text = "Press F to use";
        interactText.gameObject.SetActive(true);
    }
    void OnTriggerStay()
    {
        if(Input.GetKeyDown(KeyCode.F))
        {
            interactText.gameObject.SetActive(false);
            actionToDo.interaction();
        }
    }
    // Update is called once per frame
    void OnTriggerExit()
    {
        interactText.gameObject.SetActive(false);
    }
}
