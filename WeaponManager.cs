using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    //"80s Generic police car - Low poly model" (https://skfb.ly/6Vzwo) by Daniel Zhabotinsky is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).
    //"Kriss Vector Animated( Free)" (https://skfb.ly/6RKL7) by BarcodeGames is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).
    //"Grave Stone Collection" (https://skfb.ly/6SntP) by Kigha is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).
    public GameObject weapon1;
    public GameObject weapon2;
    public Animator anim1;
    public Animator anim2;
    void Start()
    {
        weapon1.SetActive(true);
        weapon2.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            weapon1.SetActive(true);
            anim1.Play("Draw");
            weapon2.SetActive(false);
        }
        else if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            weapon1.SetActive(false);
            weapon2.SetActive(true);
            anim2.Play("Draw");
        }
    }
}
