using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : postac
{
    // Start is called before the first frame update
    public float wanderRadius;
    public float wanderTimer;
 
    private Transform target;
    private UnityEngine.AI.NavMeshAgent agent;
    private float timer;
    public GameObject player;
    public GameObject[] listOfEnemy;
    public List<GameObject> enemies;
    int enemyIndex;
    // Use this for initialization
    void OnEnable () 
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
        timer = wanderTimer;
    }
    void Start()
    {
        findEnemy();
    }
    // Update is called once per frame
    void Update () 
    {
        enemyIndex = findClosest();
        if(enemyIndex == -1)
        {
            timer += Time.deltaTime;
 
            if (timer >= wanderTimer) 
            {   
                Vector3 newPos = RandomNavSphere(player.transform.position, wanderRadius, -1);
                agent.SetDestination(newPos);
                timer = 0;
            }
        }
        else
        {
            agent.SetDestination(enemies[enemyIndex].transform.position);
        }
        
    }
 
    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask) 
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;
 
        randDirection += origin;
 
        UnityEngine.AI.NavMeshHit navHit;
 
        UnityEngine.AI.NavMesh.SamplePosition (randDirection, out navHit, dist, layermask);
 
        return navHit.position;
    }
    void findEnemy()
    {
        listOfEnemy = GameObject.FindGameObjectsWithTag("Enemy");
        enemies = new List<GameObject>();
        foreach(GameObject go in listOfEnemy)
        {
            enemies.Add(go);
        }
    }
    int findClosest()
    {
        int index = -1;
        int i = 0;
        float enemyDistance = 20.0f;
        foreach(GameObject go in enemies)
        {
            if(Vector3.Distance(transform.position, go.transform.position) < enemyDistance)
            {
                index = i;
            }
            i++;
        }
        return index;
    }
}
