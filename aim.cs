using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aim : MonoBehaviour
{
    public Camera Camera1;
    public Camera Camera2;
    public FirstPersonAIO script;
    // Start is called before the first frame update


    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire2"))
        {
            
            Camera1.gameObject.SetActive(false);
            Camera2.gameObject.SetActive(true);
            script.playerCamera = Camera2;
        }
        else
        {
            Camera1.gameObject.SetActive(true);
            Camera2.gameObject.SetActive(false);
            script.playerCamera = Camera1;
        }
    }
}
