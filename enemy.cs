using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class enemy : MonoBehaviour
{
    public float wanderRadius;
    public float wanderTimer;
    // Start is called before the first frame update
    public Transform transformToFollow;
    //NavMesh Agent variable
    public NavMeshAgent agent;
    float distance;
    private float timer;
    private Animator anim;//<-- TO
    public hp playerHp;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.speed = 1.0f;
        anim = GetComponent<Animator>();//<-- TO
        timer = wanderTimer;
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("speed", 5.0f);//<-- TO
        distance = Vector3.Distance(transformToFollow.position, transform.position);
        //Follow the player
        if(distance < 3.0f)
        {
            timer += Time.deltaTime;
 
            if (timer >= 2.5f)
            {
                anim.SetBool("attack", true);
                playerHp.takeDamage(10);
                timer = 0;
            }
        }
        else if(distance < 100.0f)
        {
            timer = 0;
            anim.SetBool("attack", false);
            anim.SetFloat("speed", 5.0f);
            agent.destination = transformToFollow.position;
        }
        else if(distance >= 100.0f)
        {
            anim.SetFloat("speed", 5.0f);
             timer += Time.deltaTime;
 
            if (timer >= wanderTimer) {
                Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
                agent.SetDestination(newPos);
                timer = 0;
            }
        }
        
    }
    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask) {
        Vector3 randDirection = Random.insideUnitSphere * dist;
        randDirection += origin;
        NavMeshHit navHit;
        NavMesh.SamplePosition (randDirection, out navHit, dist, layermask);
 
        return navHit.position;
    }
}
