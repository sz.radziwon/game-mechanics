using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(AudioSource))]
public class shooting : MonoBehaviour
{
    Vector3 fwd;

    RaycastHit hit;
    float range = 1000.0f;
    public AudioClip shoot;
    public AudioClip reloadAudio;
    AudioSource audioSource;
    public float shotDelay = 0.5f;
    private float shotDelayCounter = 0.0f;
    public Animator anim;
    public int ammunition;
    public int ammoInMag;
    public int maxAmmo;
    public int actuallyAmmo;
    public Text AmmoText;
    void Start()
    {
       audioSource = GetComponent<AudioSource>();
    }

    void Update () 
    {
        fwd = transform.TransformDirection(Vector3.forward);
 
        if(Input.GetMouseButton(0) && shotDelayCounter <= 0.0f && ammunition > 0)
        {
            shotDelayCounter = shotDelay;
            anim.Play("Shoot");
            ammunition--;
            AmmoText.text = ammunition.ToString() + " / " + actuallyAmmo.ToString();
            //Rec.Fire();
            audioSource.PlayOneShot(shoot, 0.7F);
            Debug.DrawRay(transform.position, fwd, Color.red);
 
            if (Physics.Raycast(transform.position, fwd, out hit)) {
                
                if(hit.transform.tag == "Enemy" && hit.distance < range) {
                    
                    Destroy(hit.transform.gameObject);
                    //spawnerScript.currentEnemyCount--;
                    Debug.Log ("Trafiony przeciwnik");
                    //spawnerScript.killedEnemy++;
                }
                else if(hit.distance < range) {
                    Debug.Log ("Pudlo");
                }
            }
        }
        if(shotDelayCounter > 0)
        {
	        shotDelayCounter -= Time.deltaTime;
        }
        if((Input.GetKeyDown(KeyCode.R)) || (Input.GetMouseButton(0) && ammunition == 0&& shotDelayCounter <= 0.0f)) 
        {
            shotDelayCounter = 3.0f;
            StartCoroutine(Reload());
        }
    }
    IEnumerator Reload()
    {
        
        if(actuallyAmmo >= ammoInMag)
        {
            actuallyAmmo=actuallyAmmo - (ammoInMag - ammunition);
            ammunition=ammoInMag;
            
        }
        else
        {
            ammunition+=actuallyAmmo;
            actuallyAmmo = 0;
        }
        anim.Play("Reload");
        audioSource.PlayOneShot(reloadAudio, 0.7f);
        yield return new WaitForSeconds(3);
        
        AmmoText.text = ammunition.ToString() + " / " + actuallyAmmo.ToString();
    }
}
